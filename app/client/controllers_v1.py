import math
import jwt

from flask import Blueprint, request, jsonify
from flask_jwt import jwt_required, current_identity
from passlib.handlers.pbkdf2 import pbkdf2_sha512

from app import roles_accepted, User, db

mod_client_v1 = Blueprint('client_v1', __name__, url_prefix='/v1.0/client')


@mod_client_v1.route('/accept', methods=['GET'])
def client_invite_get():
    data = request.json
    id = jwt.decode(data['token'], 'invite', algorithms=['HS512'])['id']

    if not data['id'] or not User.query.get(data['id']).active:
        return jsonify(
            msg='Esse convite está expirado'
        )
    else:
        user = User.query.get(data['id'])
        user.password = pbkdf2_sha512.hash(data['password'])
        del data['password']
        user.extra = {**data}
