from app import Base, db
from flask_security import UserMixin, RoleMixin


class RolesUsers(Base):
    __tablename__ = 'user_profile'

    user_id = db.Column(db.Integer(), db.ForeignKey('auth_user.id'))
    profile_id = db.Column(db.Integer(), db.ForeignKey('auth_profile.id'))


class Profile(Base, RoleMixin):
    __tablename__ = 'auth_profile'

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))


class User(Base, UserMixin):
    __tablename__ = 'auth_user'

    email = db.Column(db.String, unique=True, nullable=False)
    nickname = db.Column(db.String, unique=True)
    password = db.Column(db.String)
    termos = db.Column(db.Boolean, default=False)
    roles = db.relationship('Profile', secondary=RolesUsers.__table__,
                            backref=db.backref('users', lazy='dynamic'))
