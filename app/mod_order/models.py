from app import Base, db


class Category(Base):
    __tablename__ = 'product_category'

    products = db.relationship('Product', back_populates='category')


class Product(Base):
    __tablename__ = 'product_product'

    category_id = db.Column(db.Integer, db.ForeignKey('product_category.id'), nullable=False)

    category = db.relationship('Category', back_populates='products')
    orders = db.relationship('Order', back_populates='product')


class Order(Base):
    __tablename__ = 'product_order'

    product_id = db.Column(db.Integer, db.ForeignKey('product_product.id'), nullable=False)
    product = db.relationship('Product', back_populates='orders')
