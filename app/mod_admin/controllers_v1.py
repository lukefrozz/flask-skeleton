import math
import jwt

from flask import Blueprint, request, jsonify
from flask_jwt import jwt_required, current_identity

from app import roles_accepted, User, db

mod_admin_v1 = Blueprint('admin_v1', __name__, url_prefix='/v1.0/admin')


@mod_admin_v1.route('/user', methods=['GET'])
@jwt_required()
@roles_accepted(['Administrador'])
def admin_user_get():
    if request.args.get('t') == 'all':
        users = User.query.filter(
            User.active,
        ).order_by(User.extra['name'])

        return jsonify(
            users=[{
                'id': user.id,
                'name': user.extra['name'],
            } for user in users]
        )
    pagina = 1
    if request.args.get('p'):
        pagina = int(request.args.get('p'))
    users = User.query.filter(
        User.active
    ).order_by('name').paginate(pagina, 10, False)

    return jsonify(
        users=[{
            'id': user.id,
            'name': user.name,
        } for user in users.items],
        pages=math.ceil(users.total / 10)
    )


@mod_admin_v1.route('/user/<int:_id>', methods=['GET'])
@jwt_required()
@roles_accepted(['Administrador'])
def admin_user_get_id(_id: int):
    user: User = User.query.get(_id)
    data = {
        **user.extra,
        'email': user.email,
        'nickname': user.nickname,
    }


@mod_admin_v1.route('/user/invite', methods=['POST'])
@jwt_required()
@roles_accepted(['Administrador'])
def admin_user_invite_post():
    user: User = current_identity
    j_data = request.json

    new_user = User.query.filter(User.email == j_data['email']).first()
    if not new_user:
        new_user = User()
        new_user.email = j_data['email']
        del j_data['email']
        new_user.extra = j_data

        db.session.add(new_user)
        db.session.commit()

    return jsonify(
        id=new_user.id,
        token=str(jwt.encode({'id': user.uuid}, 'invite', 'HS512'))[2:-1],
        **new_user.extra
    )
