from getpass import getpass
from fabric import Connection, Config

sudo_pass = getpass('deepin')
config = Config(overrides={
    'password': sudo_pass
})
c = Connection(host='192.168.0.39', user='deepin', port=22, config=config)


def define_primary_data(c):
    c.run('source venv/bin/activate')
    c.run('python init_db.py')


def pip_install(c):
    c.run('source venv/bin/activate')
    c.run('pip install -r requirements.txt')


def init_config(c):
    c.run('cd sim/sim-flask')
    c.run('virtualenv -p python3 venv')
    pip_install(c)


def git_pull(c):
    c.run('cd sim/sim-flask')
    c.run('git pull')
    pip_install(c)
