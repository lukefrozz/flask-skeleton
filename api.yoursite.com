server {
    server_name yoursite api.yoursite.com;

    location / {
        include uwsgi_params;
        uwsgi_pass unix:/pah/to/application/app.sock;
    }

    location /static {
       alias /path/to/application/app/static;
    }

    location /templates {
       alias /path/to/application/app/templates;
    }

}
