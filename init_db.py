#!venv/bin/python
from app import db, user_datastore
from app.mod_auth.models import Profile, User

from passlib.hash import pbkdf2_sha512
from datetime import datetime, timedelta

p_admin = Profile()
p_client = Profile()

# Profiles Creation
def create_profiles():
    print('Creating profiles')

    p_admin.name = 'Administrador'
    p_admin.extra = {'icon': 'work', 'route': '/admin/home'}
    p_client.name = 'Investidor'
    p_client.extra = {'icon': 'person', 'route': '/investor/home'}

    db.session.add(p_admin)
    db.session.add(p_client)

    db.session.commit()
    print('Profiles created')


# Admin Users
def create_admin_users():
    print('Creating admin users')
    # Adam
    adam = User()
    adam.email = 'adamsergio@gmail.com'
    adam.phone = '+556899996096660'
    adam.cpf_cnpj = '00633588245'
    adam.extra = {'name': 'Adam Sérgio Marinheiro Rocha', 'agent': {}, 'terms': True, 'gender': 'M', 'address':
        {'uf': 'AC', 'cep': '69919184', 'bairro': 'Conjunto Mariana', 'cidade': 'Rio Branco', 'numero': '90',
        'logradouro': 'Rua Felicidade', 'complemento': 'Apto. 12'}, 'birthdate': '1993-07-02', 'contracts': [
        {'uuid': '204b1696-5896-4085-8c7c-05576003ab01', 'start_date': '2019-06-13', 'maturity_date': '2119-06-13'}],
        'schooling': 'ESC', 'fatherName': 'Sérgio Antonio Francalino Rocha',
        'motherName': 'Solange Firmino Marinheiro de Araújo ', 'occupation': 'Advogado', 'nacionality':
        'Brasil', 'person_type': 'PF', 'bank_details': {'id': 1, 'agency': '2359', 'account': '33680', 'agency_dv': '0',
        'account_dv': '7', 'type_account': 'CC'}, 'maritalStatus': 'S'}
    adam.password = pbkdf2_sha512.hash('123456')

    db.session.add(adam)
    db.session.commit()

    # Lucas
    lucas = User()
    lucas.email = 'lucascostagt@hotmail.com'
    lucas.phone = '+5568992078221'
    lucas.cpf_cnpj = '03039712233'
    lucas.extra = {'name': 'Lucas Cardoso da Costa', 'agent': {}, 'terms': True, 'gender': 'M', 'address': {'uf': 'AC',
        'cep': '69901328', 'bairro': 'Baixa da Colina', 'cidade': 'Rio Branco', 'numero': '383',
        'logradouro': 'Rua Francisco Ferreira'}, 'birthdate': '11/03/1995', 'contracts': [
        {'uuid': 'e6406f83-ae9b-4875-96e2-77a665d3bd97', 'start_date': '2019-06-13', 'maturity_date': '2119-06-13'}],
        'schooling': 'ESC', 'fatherName': 'Affif Arão Alves da Costa', 'motherName': 'Dayse Maria Cardoso da Costa',
        'occupation': 'Administrador', 'nacionality': 'Brasil', 'person_type': 'PF', 'bank_details': {'id': 341,
        'agency': '9893', 'account': '03159', 'account_dv': '2', 'type_account': 'CC'}, 'maritalStatus': 'S'}
    lucas.password = pbkdf2_sha512.hash('123456')

    db.session.add(lucas)
    db.session.commit()
    print('Admin users created')

    print('Setting up admin profile to users')
    user_datastore.add_role_to_user(lucas, p_admin)
    user_datastore.add_role_to_user(lucas, p_client)
    user_datastore.add_role_to_user(adam, p_admin)
    user_datastore.add_role_to_user(adam, p_client)
    user_datastore.commit()
    print('Admin profile setted to users')


create_profiles()
create_admin_users()
